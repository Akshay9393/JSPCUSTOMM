<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

  "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/random.tld" prefix="ct" %>
<html>
    <head>
        <meta http-equiv="Content-Type" 

           content="text/html; charset=UTF-8">
        <title>JSTL Custom Tag Loop Text</title>
    </head>
    <body>
        <center>

            <ct:loopText times="5">
                <h3>Loop Text!</h3>
            </ct:loopText>

        </center>
    </body>
</html>